Build and deploy pages for the [pass-team](https://gitlab.com/pass-team/pass-team) project.

# Copyright

Copyright (C) 2022 Timm C. Fitschen <t.fitschen@indiscale.com>

# Licence (GPLv3 or later)


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[
NU General Public License](./GPLv3.md) for more details.
